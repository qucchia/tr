<!DOCTYPE html>
<html>
<head>
  <title>Alumnes &mdash; Sortin</title>
  <link rel="stylesheet" href="/main.css" />
  <link rel="icon" href="/imatges/icona.jpeg" />
</head>
<body>
  <script src="index.js"></script>
  <main>
    <noscript><p><strong>Nota</strong>: JavaScript està desactivat.</p></noscript>
        <?php
          try {
            include "../sql-login.php";
            $sql = "SELECT * FROM Alumnes;";
            $stmt = $conn->prepare($sql);
            $stmt->execute();

            $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
            $i = 0;
            foreach($stmt->fetchAll() as $fila) {
                $nom = $fila["Nom"];
                $cognom = $fila["Cognom"];
                $curs = $fila["Curs"];
                $classe = $fila["Classe"];
                $qr = "[\"$nom\", \"$cognom\", \"$curs\", \"$classe\"]";
                $image = "/fotos/" . $fila["Imatge"];
                $qr_image = "https://api.qrserver.com/v1/create-qr-code/?size=300x300&data=$qr";
                echo
                  "<div class='alumne'>" .
                    "<header>" .
                    "<a href='$image' target='_blank'><img src='$image'/></a>" .
                    "<button onClick=\"mostraOAmaga('extra-$i')\">$cognom, $nom (<i>$curs $classe</i>)</button>" .
                    "</header>" .
                    "<div class='alumne-extra' id='extra-$i'>" .
                    ($fila["PotSortir"] ?
                      '<td><div class="al-pati">' .
                        "<span>" . ($fila["AlPati"] ? "Al pati" : "A dins") . "</span>" .
                        "<button onclick=\"entraOSurt(event, '$nom', '$cognom', '$curs', '$classe')\">" . ($fila["AlPati"] ? "Entra a classe" : "Surt al pati" ) . "</button>" .
                      "</div></td>"
                     :
                     "<td>No pot sortir.</td>") .
                    "<td><a href='$qr_image' target='_blank'><img src='$qr_image'/></a></td>" .
                    "</div>" .
                  "</div>";
                $i = $i + 1;
            }
            echo $stmt->fetchAll()[0]["Nom"];
          } catch(PDOException $e) {
            echo "Error: ", $e->getMessage();
          }
        ?>
  </main>
</body>
</html>

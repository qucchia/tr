<!DOCTYPE html>
<html>

<head>
  <title>iSurt</title>
  <link rel="stylesheet" href="/main.css" />
  <link rel="icon" href="/imatges/icona.png" />
</head>

<body>
  <main>
    <header>
      <?php echo file_get_contents("./imatges/icona.svg"); ?>
      <h1>iSurt</h1>
    </header>
    <button>
      <a href="./iSurt.apk" download>Descarrega&apos;t l&apos;aplicaci&oacute;</a>
    </button>
    <form action="./alumnes">
      <h2>Taula d&apos;alumnes</h2>
      <p>
        Nom&eacute;s els comptes de professorat poden accedir la taula
        d&apos;alumnes.
      </p>
      <input type="text" name="usuari" id="usuari" placeholder="Nom d'usuari" />
      <br />
      <input type="password" name="contrasenya" id="contrasenya" placeholder="Contrasenya" />
      <br />
      <button type="submit">Accedir la taula d&apos;alumnes</button>
    </form>
  </main>
</body>

</html>

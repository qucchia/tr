<?php
include "./sql-login.php";

$query = "WHERE 1=1 AND " .
    (isset($_GET["nom"]) ? ("Nom='" . $_GET["nom"] . "' AND ") : "") .
    (isset($_GET["cogcognom"]) ? ("Cognom='" . $_GET["cognom"] . "' AND ") : "") .
    (isset($_GET["curs"]) ? ("Curs='" . $_GET["curs"] . "' AND ") : "") .
    (isset($_GET["classe"]) ? ("Classe='" . $_GET["classe"] . "' AND ") : "") .
    "1=1";

$sql = "SELECT * FROM Alumnes $query";
$stmt = $conn->prepare($sql);
$stmt->execute();

$stmt->setFetchMode(PDO::FETCH_ASSOC);
$result = $stmt->fetchAll();

if (count($result) === 0) {
    echo '{"error": "Aquest alumne/a no existeix."}';
    return;
} else if (count($result) > 1) {
    echo '{"error": "S\'ha trobat més d\'un alumne amb aquests paràmetres."}';
    return;
}

$alumne = $result[0];
?>

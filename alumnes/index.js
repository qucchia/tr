function entraOSurt(e, nom, cognom, curs, classe) {
  e.target.disabled = true;
  fetch(
    "http://" +
      document.domain +
      "/entra-o-surt-del-pati.php" +
      window.location.search +
      "&nom=" +
      nom +
      "&cognom=" +
      cognom +
      "&curs=" +
      encodeURI(curs) +
      "&classe=" +
      classe
  )
    .then((response) => response.text())
    .then((response) => {
      document.getElementById("response").innerText = response;
      const fora = response === "L'alumne ara està a fora.";
      e.target.innerText = fora ? "Entra a classe" : "Surt al pati";
      e.target.parentElement.children[0].innerText = fora ? "Sí" : "No";
      e.target.disabled = false;
    });
}

<!DOCTYPE html>
<html>

<head>
    <title>Alumnes &mdash; iSurt</title>
    <link rel="stylesheet" href="/main.css" />
    <link rel="icon" href="/imatges/icona.png" />
</head>

<body>
    <script src="index.js"></script>
    <main>
        <header>
            <?php echo file_get_contents("../imatges/icona.svg"); ?>
            <h1><a href="/">iSurt</a> &middot; Alumnes</h1>
        </header>
        <noscript>
            <p><strong>Nota</strong>: JavaScript està desactivat.</p>
        </noscript>
        <?php
        try {
            include "../sql-login.php";

            if (!$_GET["usuari"]) {
                http_response_code(403);
                echo "Contrasenya o usuari incorrectes";
                return;
            }

            $query = "WHERE " .
                "Usuari='" . $_GET["usuari"] . "' AND " .
                "Contrasenya='" . $_GET["contrasenya"] . "'";

            $sql = "SELECT * FROM Professors $query;";
            $stmt = $conn->prepare($sql);
            $stmt->execute();

            $stmt->setFetchMode(PDO::FETCH_ASSOC);
            $result = $stmt->fetchAll();

            if (count($result) == 0) {
                echo "No pots accedir la taula d'alumnes.";
                return;
            }

            echo "<table>" .
                "<thead>" .
                "<tr>" .
                "<th>Imatge</th>" .
                "<th>Nom</th>" .
                "<th>Curs</th>" .
                "<th>Classe</th>" .
                "<th>Al pati?</th>" .
                "<th>Codi QR</th>" .
                "</tr>" .
                "</thead>" .
                "<tbody>";

            $sql = "SELECT * FROM Alumnes;";
            $stmt = $conn->prepare($sql);
            $stmt->execute();

            $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
            foreach ($stmt->fetchAll() as $fila) {
                $nom = $fila["Nom"];
                $cognom = $fila["Cognom"];
                $curs = $fila["Curs"];
                $classe = $fila["Classe"];
                $dades = array(
                    "nom" => $nom, "cognom" => $cognom,
                    "curs" => $curs, "classe" => $classe
                );
                $qr = urlencode(json_encode($dades));
                $qr_image = "https://api.qrserver.com/v1/create-qr-code/?size=300x300&data=$qr";
                $image = "/fotos/" . $fila["Imatge"];
                echo
                "<tr>" .
                    "<td><a href='$image' target='_blank'><img src='$image'/></a></td>" .
                    "<td>$cognom, $nom</td>" .
                    "<td>$curs</td>" .
                    "<td>$classe</td>" .
                    ($fila["PotSortir"] ?
                        '<td><div class="al-pati">' .
                        "<span>" . ($fila["AlPati"] ? "Sí" : "No") . "</span>" .
                        "<button onclick=\"entraOSurt(event, '$nom', '$cognom', '$curs', '$classe')\">" . ($fila["AlPati"] ? "Entra a classe" : "Surt al pati") . "</button>" .
                        "</div></td>"
                        :
                        "<td>No pot sortir.</td>") .
                    "<td><a href='$qr_image' target='_blank'><img src='$qr_image'/></a></td>" .
                    "</tr>";
            }
            echo $stmt->fetchAll()[0]["Nom"];

            echo "</tbody>" .
                "</table>" .
                "<p id='response'></p>";
        } catch (PDOException $e) {
            echo "Error: ", $e->getMessage();
        }
        ?>
    </main>
</body>

</html>
